# re3data eval dashboard

Dashboard to retrieve supported standards info from re3data for the NFDI Taskforce Metadata

See notebook output here: https://nfdi4earth.pages.rwth-aachen.de/knowledgehub/re3data-eval-dashboard/

## How to use:

```
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
jupyter notebook
```

Convert notebook output to static HTML: `jupyter nbconvert  --to html --no-input repository-evaluation.ipynb`

TODO: Consider removing the output from the notebook via pre-commit hook for a cleaner git history. In that case generate a second artifact for the static pages where also the code is visible (currently we generate HTML only with the notebook's output via `--no-input`).

Author: Jonas Grieb (NFDI4Earth, SGN)
License: Apache-2.0
